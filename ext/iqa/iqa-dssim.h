/* Image Quality Assessment plugin
 * Copyright (C) 2015 Mathieu Duponchelle <mathieu.duponchelle@collabora.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_IQADSSIM_H__
#define __GST_IQADSSIM_H__

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideoaggregator.h>

G_BEGIN_DECLS

#define GST_TYPE_IQADSSIM (gst_iqadssim_get_type())
G_DECLARE_FINAL_TYPE(GstIqaDssim, gst_iqadssim, GST, IQADSSIM, GstVideoAggregator)

struct _GstIqaDssim
{
  GstVideoAggregator videoaggregator;

  gdouble ssim_threshold;
  gdouble max_dssim;
};

G_END_DECLS
#endif /* __GST_IQADSSIM_H__ */

