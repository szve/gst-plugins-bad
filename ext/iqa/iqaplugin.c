/* Image Quality Assessment plugin
 * Copyright (C) 2019 Sergey Zvezdakov <szvezdakov@graphics.cs.msu.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gst/gst.h>
#ifdef HAVE_DSSIM
#include "iqa-dssim.h"
#endif
#ifdef HAVE_LIBVMAF
#include "iqa-vmaf.h"
#endif

static gboolean
plugin_init (GstPlugin * plugin)
{
  gboolean result = TRUE;

#ifdef HAVE_DSSIM
  result &=
      gst_element_register (plugin, "iqa-dssim", GST_RANK_PRIMARY,
      GST_TYPE_IQADSSIM);
#endif
#ifdef HAVE_LIBVMAF
  result &=
      gst_element_register (plugin, "iqa-vmaf", GST_RANK_PRIMARY,
      GST_TYPE_IQAVMAF);
#endif

  return result;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    iqa,
    "Iqa", plugin_init, VERSION, GST_LICENSE, GST_PACKAGE_NAME,
    GST_PACKAGE_ORIGIN)
