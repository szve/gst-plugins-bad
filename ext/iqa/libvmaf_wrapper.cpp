/* VMAF Image Quality Assessment plugin
 * Copyright (C) 2019 Sergey Zvezdakov <szvezdakov@graphics.cs.msu.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#include <libvmaf.h>
#include <cpu.h>
#include "libvmaf_wrapper.h"

extern enum vmaf_cpu cpu;

gboolean
RunVMAF(VMAFReadingFunction read_frame, void *user_data,
  GstIqaVmafThreadHelper * thread_helper)
{
  int width = thread_helper->frame_width;
  int height = thread_helper->frame_height;
  const char * model_path = thread_helper->gst_iqavmaf_p->model_filename;
  const char * fmt;
  GstIqaVmafPoolMethodEnum pool_method = thread_helper->gst_iqavmaf_p->pool_method;
  int n_subsample = thread_helper->gst_iqavmaf_p->subsample;

  if (thread_helper->y10bit)
    fmt = "yuv420p10le";
  else
    fmt = "yuv420p";

  if (thread_helper->gst_iqavmaf_p->vmaf_config_disable_avx)
  {
    cpu = VMAF_CPU_NONE;
  } else {
    cpu = cpu_autodetect();
  }

  Result result;
  try {
    Asset asset(width, height, fmt);
    std::unique_ptr<IVmafQualityRunner> runner_ptr =
        VmafQualityRunnerFactory::createVmafQualityRunner(
        model_path, thread_helper->gst_iqavmaf_p->vmaf_config_conf_int);
    result = runner_ptr->run(asset, read_frame, user_data,
        thread_helper->gst_iqavmaf_p->vmaf_config_disable_clip,
        (thread_helper->gst_iqavmaf_p->vmaf_config_enable_transform
          || thread_helper->gst_iqavmaf_p->vmaf_config_phone_model),
        thread_helper->gst_iqavmaf_p->vmaf_config_psnr,
        thread_helper->gst_iqavmaf_p->vmaf_config_ssim,
        thread_helper->gst_iqavmaf_p->vmaf_config_ms_ssim,
        thread_helper->gst_iqavmaf_p->num_threads,
        n_subsample);
  }
  catch (VmafException& e)
  {
    thread_helper->error_msg = g_strconcat ("caughted VmafException - ",
        e.what(), NULL);
    return FALSE;
  }
  catch (std::runtime_error& e)
  {
    thread_helper->error_msg = g_strconcat ("caughted runtime_error - ",
        e.what(), NULL);
    return FALSE;
  }
  catch (std::logic_error& e)
  {
    thread_helper->error_msg = g_strconcat ("caughted logic_error - ",
        e.what(), NULL);
    return FALSE;
  }
  catch (std::exception& e)
  {
    thread_helper->error_msg = g_strconcat ("caughted exception - ",
        e.what(), NULL);
    return FALSE;
  }
  catch (...)
  {
    thread_helper->error_msg = g_strconcat ("caughted Unknown exception!",
        NULL);
    return FALSE;
  }
  switch (pool_method) {
    case MIN_POOL_METHOD:
      result.setScoreAggregateMethod(ScoreAggregateMethod::MINIMUM);;
      break;
    case MEAN_POOL_METHOD:
      result.setScoreAggregateMethod(ScoreAggregateMethod::MEAN);
      break;
    case HARMONIC_MEAN_POOL_METHOD:
      result.setScoreAggregateMethod(ScoreAggregateMethod::HARMONIC_MEAN);
      break;
  }
  std::vector<std::string> result_keys = result.get_keys();

  uint num_frames_subsampled = result.get_scores("vmaf").size();
  for (uint i_subsampled=0; i_subsampled<num_frames_subsampled; i_subsampled++)
  {
    GstStructure *frame_result_str = gst_structure_new_empty ("IQA-VMAF");
    GstMessage *msg = gst_message_new_element (GST_OBJECT (thread_helper->gst_iqavmaf_p), frame_result_str);
    GstStructure *metrics_data_str = gst_structure_new_empty ("metrics");
    gst_structure_set (frame_result_str, "padname", G_TYPE_STRING, thread_helper->padname, NULL);
    gst_structure_set (frame_result_str, "frame_num", G_TYPE_UINT, i_subsampled * n_subsample, NULL);
    for (size_t j = 0; j < result_keys.size(); j++)
    {
      double value = result.get_scores(result_keys[j].c_str()).at(i_subsampled);
      gst_structure_set (metrics_data_str, result_keys[j].c_str(), G_TYPE_DOUBLE, value, NULL);
    }
    gst_structure_set (frame_result_str, "metrics", GST_TYPE_STRUCTURE, metrics_data_str, NULL);
    gst_structure_free (metrics_data_str);
    gst_element_post_message (GST_ELEMENT (thread_helper->gst_iqavmaf_p), msg);
  }
  return TRUE;
}
