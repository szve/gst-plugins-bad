/* VMAF Image Quality Assessment plugin
 * Copyright (C) 2019 Sergey Zvezdakov <szvezdakov@graphics.cs.msu.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-iqa-vmaf
 * @title: iqa-vmaf
 * @short_description: VMAF Image Quality Assessment plugin.
 *
 * iqa-vmaf will perform full reference image quality assessment, with the
 * first added pad being the reference.
 *
 * It will perform comparisons on video streams with the same geometry.
 *
 * The image output will be a copy of the reference pad.
 *
 * The plugin requires a model file with pretrained LibSVM coefficients.
 * By default, plugin search file with "vmaf_v0.6.1.pkl" filename.
 * You can set the absolute path to the model file using
 * 'model-filename' property.
 *
 * For each reference frame, iqa-vmaf will post a message containing
 * a structure named IQA-VMAF. Important note: All messages with per-frame results will
 * be posted after EOS because of libvmaf mechanisms.
 *
 * For example, if there is one compared stream,
 * the emitted structure will look like this:
 *
 * IQA-VMAF, padname=(string)sink_1, frame_num=(uint)0, metrics=(structure)"metrics\,\
 * adm2\=\(double\)0.96487284080576141\,\ motion2\=\(double\)0\,\
 * vif_scale0\=\(double\)0.43687698390868496\,\ vif_scale1\=\(double\)0.90850678076133007\,\
 * vif_scale2\=\(double\)0.95736908154084577\,\ vif_scale3\=\(double\)0.97432499094076475\,\
 * vmaf\=\(double\)86.273788213561033\;";
 *
 * ## Example launch line
 * |[
 * gst-launch-1.0 -m uridecodebin uri=file:///test/file/1 ! iqa-vmaf name=iqa-vmaf \
 * ! videoconvert ! autovideosink uridecodebin uri=file:///test/file/2 ! iqa-vmaf.
 * ]| This pipeline will output messages to the console for each set of compared frames.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include "libvmaf_wrapper.h"

GST_DEBUG_CATEGORY_STATIC (gst_iqavmaf_debug);
#define GST_CAT_DEFAULT gst_iqavmaf_debug
#define SINK_FORMATS " { I420, YV12, Y41B, Y42B, Y444, I420_10LE, I422_10LE, Y444_10LE } "
#define SRC_FORMAT " { I420, YV12, Y41B, Y42B, Y444, I420_10LE, I422_10LE, Y444_10LE } "
#define DEFAULT_MODEL_FILENAME   "vmaf_v0.6.1.pkl"
#define DEFAULT_DISABLE_CLIP     FALSE
#define DEFAULT_DISABLE_AVX      FALSE
#define DEFAULT_ENABLE_TRANSFORM FALSE
#define DEFAULT_PHONE_MODEL      FALSE
#define DEFAULT_PSNR             FALSE
#define DEFAULT_SSIM             FALSE
#define DEFAULT_MS_SSIM          FALSE
#define DEFAULT_POOL_METHOD      MEAN_POOL_METHOD
#define DEFAULT_NUM_THREADS      0
#define DEFAULT_SUBSAMPLE        1
#define DEFAULT_CONF_INT         FALSE
#define GST_TYPE_IQAVMAF_POOL_METHOD (gst_iqavmaf_pool_method_get_type ())

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (SRC_FORMAT))
    );

enum
{
  PROP_0,
  PROP_MODEL_FILENAME,
  PROP_DISABLE_CLIP,
  PROP_DISABLE_AVX,
  PROP_ENABLE_TRANSFORM,
  PROP_PHONE_MODEL,
  PROP_PSNR,
  PROP_SSIM,
  PROP_MS_SSIM,
  PROP_POOL_METHOD,
  PROP_NUM_THREADS,
  PROP_SUBSAMPLE,
  PROP_CONF_INT,
  PROP_LAST,
};

enum VMAFReadFuncRetCodes
{
  VMAF_SUCCESSFUL_READING = 0,
  VMAF_READING_FAILED = 2
};


static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink_%u",
    GST_PAD_SINK,
    GST_PAD_REQUEST,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (SINK_FORMATS))
    );

static GType
gst_iqavmaf_pool_method_get_type (void)
{
  static const GEnumValue types[] = {
    {MIN_POOL_METHOD, "Minimum value", "min"},
    {MEAN_POOL_METHOD, "Arithmetic mean", "mean"},
    {HARMONIC_MEAN_POOL_METHOD, "Harmonic mean", "harmonic_mean"},
    {0, NULL, NULL},
  };
  static gsize id = 0;

  if (g_once_init_enter (&id)) {
    GType _id = g_enum_register_static ("GstIqaVmafPoolMethod", types);
    g_once_init_leave (&id, _id);
  }

  return (GType) id;
}


/* GstIqaVmaf */

static inline float
get_data_from_ptr (void *ptr, int i, int j, int frame_width, gboolean y10bit)
{
  float result;
  if (y10bit)
    result = (float) ((guint16 *) ptr)[i * frame_width + j] / 4.0;
  else
    result = (float) ((guint8 *) ptr)[i * frame_width + j];
  return result;
}

static int
read_frame (float *ref_data, float *main_data, float *temp_data,
    int stride, void *h)
{
  GstIqaVmafThreadHelper *helper = (GstIqaVmafThreadHelper *) h;
  int ret;
  GstIqaVmafQueueElem *frames_data;
  frames_data = g_async_queue_pop (helper->frame_queue);
  if (frames_data->original_ptr && frames_data->distorted_ptr) {
    int i, j;
    float *ref_ptr = ref_data;
    float *main_ptr = main_data;
    for (i = 0; i < helper->frame_height; i++) {
      for (j = 0; j < helper->frame_width; j++) {
        ref_ptr[j] = get_data_from_ptr (frames_data->original_ptr, i, j,
            helper->frame_width, helper->y10bit);
        main_ptr[j] = get_data_from_ptr (frames_data->distorted_ptr, i, j,
            helper->frame_width, helper->y10bit);
      }
      ref_ptr += stride / sizeof (*ref_data);
      main_ptr += stride / sizeof (*ref_data);
    }
    ret = VMAF_SUCCESSFUL_READING;
  } else {
    ret = VMAF_READING_FAILED;
  }
  if (frames_data) {
    g_free (frames_data->original_ptr);
    g_free (frames_data->distorted_ptr);
  }
  g_free (frames_data);
  return ret;
}

#define gst_iqavmaf_parent_class parent_class
G_DEFINE_TYPE (GstIqaVmaf, gst_iqavmaf, GST_TYPE_VIDEO_AGGREGATOR);

static void
vmaf_thread_call (void *vs)
{
  GstIqaVmafThreadHelper *helper;
  gboolean thread_is_stopped = FALSE;
  gboolean ret_status;
  if (vs == NULL)
    return;
  helper = (GstIqaVmafThreadHelper *) vs;
  g_mutex_lock (&helper->check_thread_failure);
  thread_is_stopped = helper->thread_failure;
  g_mutex_unlock (&helper->check_thread_failure);
  if (thread_is_stopped)
    return;
  ret_status = RunVMAF (read_frame, vs, helper);
  g_mutex_lock (&helper->check_thread_failure);
  helper->thread_failure = !ret_status;
  g_mutex_unlock (&helper->check_thread_failure);
  if (helper->error_msg)
    GST_ELEMENT_ERROR (helper->gst_iqavmaf_p, RESOURCE, FAILED,
        ("Launhing LibVMAF error: %s", helper->error_msg),
        ("Launhing LibVMAF error: %s", helper->error_msg));
}

static gboolean
try_thread_stop (GstTask * thread)
{
  GstTaskState task_state;
  gboolean result;
  task_state = gst_task_get_state (thread);
  if (task_state == GST_TASK_STARTED) {
    result = gst_task_stop (thread);
  } else {
    result = TRUE;
  }
  return result;
}

static gboolean
compare_frames (GstIqaVmaf * self, GstVideoFrame * ref, GstVideoFrame * cmp,
    GstBuffer * outbuf, GstIqaVmafThreadHelper * thread_data)
{
  gboolean result;
  GstMapInfo ref_info;
  GstMapInfo cmp_info;
  GstMapInfo out_info;
  gint frames_size;
  GstIqaVmafQueueElem *frames_data;
  // Check that thread is waiting
  g_mutex_lock (&thread_data->check_thread_failure);
  if (thread_data->thread_failure) {
    try_thread_stop (thread_data->vmaf_thread);
    return FALSE;
  }
  g_mutex_unlock (&thread_data->check_thread_failure);
  // Run reading
  gst_buffer_map (ref->buffer, &ref_info, GST_MAP_READ);
  gst_buffer_map (cmp->buffer, &cmp_info, GST_MAP_READ);
  gst_buffer_map (outbuf, &out_info, GST_MAP_WRITE);

  frames_size = thread_data->frame_height * thread_data->frame_width;
  if (thread_data->y10bit)
    frames_size *= 2;

  frames_data = g_malloc (sizeof (GstIqaVmafQueueElem));
  frames_data->original_ptr = g_memdup (ref_info.data, frames_size);
  frames_data->distorted_ptr = g_memdup (cmp_info.data, frames_size);

  g_async_queue_push (thread_data->frame_queue, frames_data);

  g_mutex_lock (&thread_data->check_thread_failure);
  if (!thread_data->thread_failure) {
    gint i;
    result = TRUE;
    for (i = 0; i < ref_info.size; i++) {
      out_info.data[i] = ref_info.data[i];
    }
  } else {
    try_thread_stop (thread_data->vmaf_thread);
    result = FALSE;
  }
  g_mutex_unlock (&thread_data->check_thread_failure);
  gst_buffer_unmap (ref->buffer, &ref_info);
  gst_buffer_unmap (cmp->buffer, &cmp_info);
  gst_buffer_unmap (outbuf, &out_info);
  return result;
}

static GstFlowReturn
gst_iqavmaf_aggregate_frames (GstVideoAggregator * vagg, GstBuffer * outbuf)
{
  GList *l;
  GstVideoFrame *ref_frame = NULL;
  GstIqaVmaf *self = GST_IQAVMAF (vagg);
  gboolean res = TRUE;
  guint stream_index = 0;

  GST_OBJECT_LOCK (vagg);
  for (l = GST_ELEMENT (vagg)->sinkpads; l; l = l->next) {
    GstVideoAggregatorPad *pad = l->data;
    GstVideoFrame *prepared_frame =
        gst_video_aggregator_pad_get_prepared_frame (pad);

    if (prepared_frame != NULL) {
      if (!ref_frame) {
        ref_frame = prepared_frame;
      } else {
        GstIqaVmafThreadHelper *thread_data =
            &self->helper_struct_pointer[stream_index];
        GstVideoFrame *cmp_frame = prepared_frame;

        res &= compare_frames (self, ref_frame, cmp_frame, outbuf, thread_data);

        ++stream_index;
      }
    }
  }
  if (!res)
    goto failed;
  GST_OBJECT_UNLOCK (vagg);

  return GST_FLOW_OK;

failed:
  GST_OBJECT_UNLOCK (vagg);

  return GST_FLOW_ERROR;
}

static void
gst_iqavmaf_set_pool_method (GstIqaVmaf * self, gint pool_method)
{
  switch (pool_method) {
    case MIN_POOL_METHOD:
      self->pool_method = MIN_POOL_METHOD;
      break;
    case MEAN_POOL_METHOD:
      self->pool_method = MEAN_POOL_METHOD;
      break;
    case HARMONIC_MEAN_POOL_METHOD:
      self->pool_method = HARMONIC_MEAN_POOL_METHOD;
      break;
    default:
      g_assert_not_reached ();
  }
}

static void
_set_property (GObject * object, guint prop_id, const GValue * value,
    GParamSpec * pspec)
{
  GstIqaVmaf *self = GST_IQAVMAF (object);

  GST_OBJECT_LOCK (self);
  switch (prop_id) {
    case PROP_MODEL_FILENAME:
      g_free (self->model_filename);
      self->model_filename = g_value_dup_string (value);
      break;
    case PROP_DISABLE_CLIP:
      self->vmaf_config_disable_clip = g_value_get_boolean (value);
      break;
    case PROP_DISABLE_AVX:
      self->vmaf_config_disable_avx = g_value_get_boolean (value);
      break;
    case PROP_ENABLE_TRANSFORM:
      self->vmaf_config_enable_transform = g_value_get_boolean (value);
      break;
    case PROP_PHONE_MODEL:
      self->vmaf_config_phone_model = g_value_get_boolean (value);
      break;
    case PROP_PSNR:
      self->vmaf_config_psnr = g_value_get_boolean (value);
      break;
    case PROP_SSIM:
      self->vmaf_config_ssim = g_value_get_boolean (value);
      break;
    case PROP_MS_SSIM:
      self->vmaf_config_ms_ssim = g_value_get_boolean (value);
      break;
    case PROP_POOL_METHOD:
      gst_iqavmaf_set_pool_method (self, g_value_get_enum (value));
      break;
    case PROP_NUM_THREADS:
      self->num_threads = g_value_get_uint (value);
      break;
    case PROP_SUBSAMPLE:
      self->subsample = g_value_get_uint (value);
      break;
    case PROP_CONF_INT:
      self->vmaf_config_conf_int = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
  GST_OBJECT_UNLOCK (self);
}

static void
_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstIqaVmaf *self = GST_IQAVMAF (object);

  GST_OBJECT_LOCK (self);
  switch (prop_id) {
    case PROP_MODEL_FILENAME:
      g_value_set_string (value, self->model_filename);
      break;
    case PROP_DISABLE_CLIP:
      g_value_set_boolean (value, self->vmaf_config_disable_clip);
      break;
    case PROP_DISABLE_AVX:
      g_value_set_boolean (value, self->vmaf_config_disable_avx);
      break;
    case PROP_ENABLE_TRANSFORM:
      g_value_set_boolean (value, self->vmaf_config_enable_transform);
      break;
    case PROP_PHONE_MODEL:
      g_value_set_boolean (value, self->vmaf_config_phone_model);
      break;
    case PROP_PSNR:
      g_value_set_boolean (value, self->vmaf_config_psnr);
      break;
    case PROP_SSIM:
      g_value_set_boolean (value, self->vmaf_config_ssim);
      break;
    case PROP_MS_SSIM:
      g_value_set_boolean (value, self->vmaf_config_ms_ssim);
      break;
    case PROP_POOL_METHOD:
      g_value_set_enum (value, self->pool_method);
      break;
    case PROP_NUM_THREADS:
      g_value_set_uint (value, self->num_threads);
      break;
    case PROP_SUBSAMPLE:
      g_value_set_uint (value, self->subsample);
      break;
    case PROP_CONF_INT:
      g_value_set_boolean (value, self->vmaf_config_conf_int);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
  GST_OBJECT_UNLOCK (self);
}

/* GObject boilerplate */

static void
gst_iqavmaf_init (GstIqaVmaf * self)
{
  GValue tmp = G_VALUE_INIT;
  g_value_init (&tmp, G_TYPE_STRING);
  g_value_set_static_string (&tmp, DEFAULT_MODEL_FILENAME);
  self->model_filename = g_value_dup_string (&tmp);
  self->vmaf_config_disable_clip = DEFAULT_DISABLE_CLIP;
  self->vmaf_config_disable_avx = DEFAULT_DISABLE_AVX;
  self->vmaf_config_enable_transform = DEFAULT_ENABLE_TRANSFORM;
  self->vmaf_config_phone_model = DEFAULT_PHONE_MODEL;
  self->vmaf_config_psnr = DEFAULT_PSNR;
  self->vmaf_config_ssim = DEFAULT_SSIM;
  self->vmaf_config_ms_ssim = DEFAULT_MS_SSIM;
  gst_iqavmaf_set_pool_method (self, DEFAULT_POOL_METHOD);
  self->num_threads = DEFAULT_NUM_THREADS;
  self->subsample = DEFAULT_SUBSAMPLE;
  self->vmaf_config_conf_int = DEFAULT_CONF_INT;
  g_mutex_init (&self->finish_mutex);
}

static void
gst_iqavmaf_finalize (GObject * object)
{
  GstIqaVmaf *self = GST_IQAVMAF (object);

  g_mutex_clear (&self->finish_mutex);

  g_free (self->model_filename);
  self->model_filename = NULL;

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gboolean
gst_iqavmaf_negotiated_src_caps (GstAggregator * agg, GstCaps * caps)
{
  GstIqaVmaf *self = GST_IQAVMAF (agg);
  gint width, height;
  const gchar *format;
  gboolean y10bit;
  GList *sinkpads_list = GST_ELEMENT (agg)->sinkpads;
  GstStructure *caps_structure = gst_caps_get_structure (caps, 0);

  gst_structure_get_int (caps_structure, "height", &height);
  gst_structure_get_int (caps_structure, "width", &width);
  format = gst_structure_get_string (caps_structure, "format");
  y10bit = g_str_has_suffix (format, "_10LE");

  self->number_of_vmaf_threads = g_list_length (GST_ELEMENT (agg)->sinkpads);
  --self->number_of_vmaf_threads;       // Without reference
  sinkpads_list = sinkpads_list->next;  // Skip reference

  self->finish_threads = FALSE;

  self->helper_struct_pointer =
      g_malloc (sizeof (GstIqaVmafThreadHelper) * self->number_of_vmaf_threads);
  for (guint i = 0; i < self->number_of_vmaf_threads; ++i) {
    self->helper_struct_pointer[i].gst_iqavmaf_p = self;
    self->helper_struct_pointer[i].thread_failure = FALSE;
    self->helper_struct_pointer[i].frame_height = height;
    self->helper_struct_pointer[i].frame_width = width;
    self->helper_struct_pointer[i].y10bit = y10bit;
    self->helper_struct_pointer[i].error_msg = NULL;
    self->helper_struct_pointer[i].padname =
        gst_pad_get_name (sinkpads_list->data);
    sinkpads_list = sinkpads_list->next;

    self->helper_struct_pointer[i].frame_queue = g_async_queue_new ();

    g_mutex_init (&self->helper_struct_pointer[i].check_thread_failure);
    g_rec_mutex_init (&self->helper_struct_pointer[i].vmaf_thread_mutex);
    self->helper_struct_pointer[i].vmaf_thread = gst_task_new (vmaf_thread_call,
        (void *) &self->helper_struct_pointer[i], NULL);
    gst_task_set_lock (self->helper_struct_pointer[i].vmaf_thread,
        &self->helper_struct_pointer[i].vmaf_thread_mutex);
    gst_task_start (self->helper_struct_pointer[i].vmaf_thread);
  }
  return GST_AGGREGATOR_CLASS (parent_class)->negotiated_src_caps (agg, caps);
}

static void
gst_iqavmaf_stop_plugin (GstAggregator * aggregator)
{
  GstIqaVmafThreadHelper *helper;
  GstIqaVmaf *self = GST_IQAVMAF (aggregator);
  g_mutex_lock (&self->finish_mutex);
  if (!self->finish_threads) {
    for (int i = 0; i < self->number_of_vmaf_threads; ++i) {
      gboolean thread_failure;
      helper = &self->helper_struct_pointer[i];
      g_mutex_lock (&helper->check_thread_failure);
      thread_failure = helper->thread_failure;
      g_mutex_unlock (&helper->check_thread_failure);
      if (thread_failure) {
        gint q_length = g_async_queue_length (helper->frame_queue);
        for (gint i = 0; i < q_length; ++i) {
          GstIqaVmafQueueElem *frames_data;
          frames_data = g_async_queue_pop (helper->frame_queue);
          if (frames_data) {
            g_free (frames_data->original_ptr);
            g_free (frames_data->distorted_ptr);
            g_free (frames_data);
          }
        }
      } else {
        GstIqaVmafQueueElem *frames_data;
        frames_data = g_malloc (sizeof (GstIqaVmafQueueElem));
        frames_data->original_ptr = NULL;
        frames_data->distorted_ptr = NULL;
        g_async_queue_push (helper->frame_queue, frames_data);
      }
    }
    for (int i = 0; i < self->number_of_vmaf_threads; ++i) {
      helper = &self->helper_struct_pointer[i];
      gst_task_join (helper->vmaf_thread);
      g_free (helper->error_msg);
      g_free (helper->padname);
      gst_object_unref (helper->vmaf_thread);
      g_rec_mutex_clear (&helper->vmaf_thread_mutex);
      g_mutex_clear (&helper->check_thread_failure);
      g_async_queue_unref (helper->frame_queue);
    }
    g_free (self->helper_struct_pointer);
    self->finish_threads = TRUE;
  }
  g_mutex_unlock (&self->finish_mutex);
}

static gboolean
gst_iqavmaf_sink_event (GstAggregator * aggregator,
    GstAggregatorPad * aggregator_pad, GstEvent * event)
{
  if (GST_EVENT_TYPE (event) == GST_EVENT_EOS)
    gst_iqavmaf_stop_plugin (aggregator);
  return GST_AGGREGATOR_CLASS (parent_class)->sink_event (aggregator,
      aggregator_pad, event);
}

static gboolean
gst_iqavmaf_stop (GstAggregator * aggregator)
{
  gst_iqavmaf_stop_plugin (aggregator);
  return TRUE;
}

static void
gst_iqavmaf_class_init (GstIqaVmafClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *gstelement_class = (GstElementClass *) klass;
  GstVideoAggregatorClass *videoaggregator_class =
      (GstVideoAggregatorClass *) klass;
  GstAggregatorClass *aggregator_class = (GstAggregatorClass *) klass;

  videoaggregator_class->aggregate_frames = gst_iqavmaf_aggregate_frames;

  gst_element_class_add_static_pad_template_with_gtype (gstelement_class,
      &src_factory, GST_TYPE_AGGREGATOR_PAD);
  gst_element_class_add_static_pad_template_with_gtype (gstelement_class,
      &sink_factory, GST_TYPE_VIDEO_AGGREGATOR_CONVERT_PAD);

  aggregator_class->sink_event = gst_iqavmaf_sink_event;
  aggregator_class->negotiated_src_caps = gst_iqavmaf_negotiated_src_caps;
  aggregator_class->stop = gst_iqavmaf_stop;

  gobject_class->set_property = GST_DEBUG_FUNCPTR (_set_property);
  gobject_class->get_property = GST_DEBUG_FUNCPTR (_get_property);
  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_iqavmaf_finalize);

  g_object_class_install_property (gobject_class, PROP_MODEL_FILENAME,
      g_param_spec_string ("model-filename", "model-filename",
          "Model *.pkl abs filename", DEFAULT_MODEL_FILENAME,
          G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_DISABLE_CLIP,
      g_param_spec_boolean ("disable-clip", "disable-clip",
          "Disable clipping VMAF values", DEFAULT_DISABLE_CLIP,
          G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_DISABLE_AVX,
      g_param_spec_boolean ("disable-avx", "disable-avx",
          "Disable AVX intrinsics using", DEFAULT_DISABLE_AVX,
          G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_ENABLE_TRANSFORM,
      g_param_spec_boolean ("enable-transform", "enable-transform",
          "Enable transform VMAF scores", DEFAULT_ENABLE_TRANSFORM,
          G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_PHONE_MODEL,
      g_param_spec_boolean ("phone-model", "phone-model",
          "Use VMAF phone model", DEFAULT_PHONE_MODEL, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_PSNR,
      g_param_spec_boolean ("psnr", "psnr",
          "Estimate PSNR", DEFAULT_PSNR, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_SSIM,
      g_param_spec_boolean ("ssim", "ssim",
          "Estimate SSIM", DEFAULT_SSIM, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_MS_SSIM,
      g_param_spec_boolean ("ms-ssim", "ms-ssim",
          "Estimate MS-SSIM", DEFAULT_MS_SSIM, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_POOL_METHOD,
      g_param_spec_enum ("pool-method", "pool-method",
          "Pool method for mean", GST_TYPE_IQAVMAF_POOL_METHOD,
          DEFAULT_POOL_METHOD,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  g_object_class_install_property (gobject_class, PROP_NUM_THREADS,
      g_param_spec_uint ("threads", "threads",
          "The number of threads",
          0, 32, DEFAULT_NUM_THREADS, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_SUBSAMPLE,
      g_param_spec_uint ("subsample", "subsample",
          "Computing on one of every N frames",
          1, 128, DEFAULT_SUBSAMPLE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_CONF_INT,
      g_param_spec_boolean ("conf-interval", "conf-interval",
          "Enable confidence intervals", DEFAULT_CONF_INT, G_PARAM_READWRITE));

  gst_element_class_set_static_metadata (gstelement_class, "iqa-vmaf",
      "Filter/Analyzer/Video",
      "Provides Video Multi-Method Assessment Fusion metric",
      "Sergey Zvezdakov <szvezdakov@graphics.cs.msu.ru>");
  GST_DEBUG_CATEGORY_INIT (gst_iqavmaf_debug, "iqa", 0, "iqa-vmaf");
}
