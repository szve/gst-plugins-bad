/* VMAF Image Quality Assessment plugin
 * Copyright (C) 2019 Sergey Zvezdakov <szvezdakov@graphics.cs.msu.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_IQAVMAF_H__
#define __GST_IQAVMAF_H__

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideoaggregator.h>

G_BEGIN_DECLS

#define GST_TYPE_IQAVMAF (gst_iqavmaf_get_type())
G_DECLARE_FINAL_TYPE(GstIqaVmaf, gst_iqavmaf, GST, IQAVMAF, GstVideoAggregator)

typedef enum _GstIqaVmafPoolMethodEnum
{
  MIN_POOL_METHOD = 0,
  MEAN_POOL_METHOD = 1,
  HARMONIC_MEAN_POOL_METHOD = 2
} GstIqaVmafPoolMethodEnum;

typedef struct {
  void *original_ptr;
  void *distorted_ptr;
} GstIqaVmafQueueElem;

typedef struct {
  GstIqaVmaf *gst_iqavmaf_p;
  GstTask *vmaf_thread;
  GRecMutex vmaf_thread_mutex;
  GAsyncQueue *frame_queue;
  GMutex check_thread_failure;
  gboolean thread_failure;
  gchar *error_msg;
  gint frame_height;
  gint frame_width;
  gboolean y10bit;
  gchar *padname;
} GstIqaVmafThreadHelper;

/**
 * GstIqaVmaf:
 *
 * The opaque #GstIqaVmaf structure.
 */
struct _GstIqaVmaf
{
  GstVideoAggregator videoaggregator;
  // VMAF settings from cmd
  gchar *model_filename;
  gboolean vmaf_config_disable_clip;
  gboolean vmaf_config_disable_avx;
  gboolean vmaf_config_enable_transform;
  gboolean vmaf_config_phone_model;
  gboolean vmaf_config_psnr;
  gboolean vmaf_config_ssim;
  gboolean vmaf_config_ms_ssim;
  GstIqaVmafPoolMethodEnum pool_method;
  guint num_threads;
  guint subsample;
  gboolean vmaf_config_conf_int;
  // Thread helpers
  GstIqaVmafThreadHelper *helper_struct_pointer;
  gint number_of_vmaf_threads;
  gboolean finish_threads;
  GMutex finish_mutex;
};

G_END_DECLS
#endif /* __GST_IQAVMAF_H__ */
